'''
computervision: Commands module
Contains all of the commands to be accessed by the CommandRegistry module.  
Contact Charlie Friend [charles.d.friend@gmail.com] for help with this module. 

Copyright 2015, UVic AERO
Licensed under MIT
'''

'''
Each command will be in the form of a class in this file - the class name will
be the name used to access the command from the System Analyst.  The command's 
functionality will be defined in the "execute" function and any text following
the command will be passed to the function in a list of arguments.

e.g. "exampleCommand x y z" will access the exampleCommand function.  
     args: [x, y, z]
'''
class exampleCommand():
	'''
	This function will be called when the command is accessed from the SA.  
	Without it, a warning will be generated on the CommandRegistry's initialization
	and the command will not run.
	'''
	def execute(args):
		print("It works!")

class printArgs():
	'''
	Prints the arguments passed to the function.
	'''
	def execute(args):
		print(args)