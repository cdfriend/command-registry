# README #

This code is a separate version of UVic AERO's command registry code for testing.  It will allow commands to be created, edited and executed from the command line.  

### How do I get set up? ###

In order to use the testing interface, simply run the file Start.py.  

Creating commands can be done in the file commands.py.  Create a class with an execute method in the file, like so:

```
#!python

class [commandName]():
     def execute(args): #"args" will contain a list of the items following the command name, separated by spaces
          #This code will be executed when the command is called from the command line.
```

The class name will be the same name used to access the command.  For example, a class called "testCommand" could be used by typing "testCommand x y z" into the command line.  "args" will contain a list of the items following the command name, in this case ['x', 'y', 'z'].

### Who do I talk to? ###

Any questions or issues with this repository can be directed to Charlie Friend [charles.d.friend@gmail.com].