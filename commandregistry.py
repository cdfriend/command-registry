'''
computervision: Command Registry module
Processes incoming command strings from the System Analyst client.  All commands and their 
source code can be created and edit in commands.py.  
Contact Charlie Friend [charles.d.friend@gmail.com] for help with this module. 

Copyright 2015, UVic AERO
Licensed under MIT
'''

import sys, inspect
import commands

class CommandRegistry(object):
	#Initializes the command registry object and finds all usable commands.
	def __init__(self):
		#create dictionary for all name/command pairs
		self.cmdReg = {}
		#get all classes in Commands.py
		members = inspect.getmembers(sys.modules["commands"], inspect.isclass)
		for name, obj in members:
			#get all methods in each detected class, check that the class contains an execute function
			cmdMembers = inspect.getmembers(obj, inspect.isfunction)
			containsExecuteFunction = False
			for methodName, methodObj in cmdMembers:
				if methodName == 'execute':
					containsExecuteFunction = True
			#Add the class to the registry if it contains an execute function, otherwise log an error message
			if containsExecuteFunction:
				self.cmdReg[name] = obj
			else:
				print("Command " + name + " could not be initialized - does not contain an execute function.")

	#Finds the command requested by an incoming JSON packet in the registry and executes the code.
	def process(self, commandString):
		#split the command string into the requested command and arguments
		splitInput = commandString.split(" ")
		if splitInput[0] in self.cmdReg:
			#if the command is found, create a list of arguments and execute the command
			#TODO: add error handling and logging capabilities here
			cmdName = splitInput[0]
			if (len(splitInput) > 1):
				splitInput.remove(cmdName)
			self.cmdReg[cmdName].execute(splitInput)
		else:
			#log that the requested command was not found in the registry
			#TODO: integrate error messages into the logging system
			print("Command not found.")
