'''
start: serves as a testing platform for the commandregistry and commands modules
Contact Charlie Friend [charles.d.friend@gmail.com] for help with this module. 

Copyright 2015, UVic AERO
Licensed under MIT
'''

from commandregistry import CommandRegistry

print("--UVic AERO Command Parsing--")
cmdReg = CommandRegistry()

while True:
	cmdReg.process(input())
